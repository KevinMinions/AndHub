[![Chat - xmpp](https://img.shields.io/badge/chat-on%20xmpp-blue.svg)](xmpp:dishub@chat.disroot.org?join)
[![Donate](https://img.shields.io/badge/donate-appreciation-orange.svg)](https://disroot.org/en/donate)
[![Donate LiberaPay](https://img.shields.io/badge/donate-liberapay-orange.svg)](https://liberapay.com/Disroot)

# Nomad - Hubzilla app for android
<img src="/app/src/main/ic_launcher-web.png" align="left" width="100" hspace="10" vspace="10">
This is an unofficial webview based client for the community-run, distributed social network <b><a href="https://project.hubzilla.org/page/hubzilla/hubzilla-project">Hubzilla</a></b>.

</br>
</br>

## Description
This is an unofficial webview based client for the community-run, distributed social network <b><a href="https://project.hubzilla.org/page/hubzilla/hubzilla-project/">Hubzilla</a></b>.
It's currently under development and should be used with that in mind. Please submit any bugs you might find.
We started Nomad as a fork of the Dandelion* app

#### WebApp
The app is developed as a WebApp

Why is a WebApp better than using the mobile site on a browser?
Basically it provides better integration with the system (events coming into and going out of the app), customized interface and functions and a nice little icon that takes you directly to your favorite social network :)

#### Device Requirements
The minimum Android version supported is Jelly Bean, Android v4.2.0 / API 17

### Privacy & Permissions<a name="privacy"></a>
Hubzilla requires access to the Internet and to external storage to be able to upload photos when creating a new post and for taking screenshots.


## Contributions
The project is always open for contributions and accepts pull requests.
The project uses [AOSP Java Code Style](https://source.android.com/source/code-style#follow-field-naming-conventions), with one exception: private members are `_camelCase` instead of `mBigCamel`. You may use Android Studios _auto reformat feature_ before sending a PR.

Translations can be contributed on Framagit. Or you can send us translations via E-Mail attachement.

Note that the main project members are working on this project for free during leisure time, are mostly busy with their job/university/school, and may not react or start coding immediately.

#### Resources
* Project: [Changelog](/CHANGELOG.md) | [Issues level/beginner](https://framagit.org/disroot/AndHub/issues) | [License](/LICENSE.md) | [CoC](/CODE_OF_CONDUCT.md)
* Project Nomad account: [not there yet](https://)
* Hubzilla: [Framagit](https://framagit.org/hubzilla/core) | [Web](https://project.hubzilla.org) | [Hub HQ account](https://project.hubzilla.org/channel/hubzilla)
* F-droid: [Nomad](https://f-droid.org/en/packages/com.dfa.hubzilla_android/)

## Licensing
Nomad is released under GNU GENERAL PUBLIC LICENSE (see [LICENCE](https://framagit.org/disroot/AndHub/blob/master/LICENSE.md)).
The app is licensed GPL v3. Localization files and resources (strings\*.xml) are licensed CC0 1.0.
For more licensing informations, see [`3rd party licenses`](/app/src/main/res/raw/licenses_3rd_party.md).

## Screenshots
WIP

### Notice

#### Maintainers
- Disroot ([Framagit](https://framagit.org/disroot), [Web](https://disroot.org), [Hubzilla](https://hub.disroot.org/channel/disroot))
