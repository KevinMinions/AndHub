<!--
This file contains references to people who contributed to the app.
You can also send a mail to [WIP](http://WIP) to get included.

Schema:  **[Name](Reference)**<br/>~° Text

Where:
  * Name: username, first/lastname
  * Reference: E-Mail, Webpage
  * Text: Information about / kind of contribution



## LIST OF CONTRIBUTORS
-->
### Contributors of dandelion:
* **[Gregor Santner](http://gsantner.net)**<br/>~° Current developer of dandelion
* **[Paul Schaub](https://github.com/vanitasvitae)**<br/>~° Development of dandelion
* **[Martín Vukovic](martinvukovic AT protonmail DOT com)**<br/>~° Diaspora Native WebApp
* **[Gaukler Faun](https://github.com/scoute-dich)**<br/>~° Diaspora Native WebApp additions
* **[Airon90](https://diasp.eu/u/airon90)**<br/>~° Italian translation
* **[Nacho Fernández](nacho_f AT joindiaspora DOT com)**<br/>~° Spanish translation
* **[Naofumi Fukue](https://github.com/naofum)**<br/>~° Japanese translation
* **[pskosinski](email AT pskosinski DOT pl)**<br/>~° Polish translation
* **[SansPseudoFix](https://github.com/SansPseudoFix)**<br/>~° French translation
* **[Zsolt Szakács](maxigaz AT diaspora DOT zone)**<br/>~° Hungarian translation
* **[Luís F.S. Rosa](https://github.com/luisfsr)**<br/>~° Brazilian Portuguese translation
* **[Danilo Raffaelli](https://crowdin.com/profile/Daraf)**<br/>~° Italian translation
* **[Âng Iōngchun](https://pubpod.alqualonde.org/u/iongchun)**<br/>~° Chinese traditional translation
* **[Mikkel Kirkgaard Nielsen](http://www.mikini.dk)**<br/>~° Danish translation
* **[Jean Lucas](jean AT 4ray DOT co)**<br/>~° Spanish translation
* **[asereze](https://github.com/asereze)**<br/>~° Sardinian translation
* **[Xosé M. Lamas](http://xmgz.eu)**<br />~° Galician translation
---
### Contributors:
**[Massimiliano](https://hub.disroot.org/channel/massimiliano)**<br/>~° Current developer of Nomad<br/>
**[gia vec](https://framagit.org/elvecio)**<br/>~° Italian Translator<br/>
**[F&#42;&#42;&#42;OffGoogle](https://framagit.org/ajeremias)**<br/>~° Logo designer<br/>
